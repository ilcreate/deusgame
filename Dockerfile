FROM node:latest as build
WORKDIR /srv
COPY . .
RUN npm install --include=dev && \
    npm run build && \
    npm cache clean --force

FROM nginx:alpine-slim
RUN apk add --update npm
WORKDIR /srv
COPY --from=build /srv .
CMD ["npm", "start"]